# Exchange Rates Application
Built on top of https://ratesapi.io/

##Getting Started

### Requirements
**Java 8**

### How to build
This uses gradle, the gradle wrapper is provided, please make sure the wrapper instead of your local gradle
 installation, this will garantee no build issues due to different versions of the build tool:

unix based: `./gradlew clean build`

for the windows folks: `gradlew.bat clean build`

The above will generate a fat runnable jar: `build/libs/exchange-rates-0.0.1-SNAPSHOT.jar`

The code was crafted on Intellij but should load just fine on Eclipse. When loading it into any of these IDEs, also
 ensure to select the provided gradle wrapper as the gradle distribution for the project.
 
 ### How to Run
 Simply run `java -jar ./build/libs/exchange-rates-0.0.1-SNAPSHOT.jar` and the server will listen on port 8080 by
  default.
  
  Note that it will perform health checks against https://ratesapi.io during startup.
  
####Steps to debug/browse the code on Intellij: 
* Enable annotations processing for Lombok
* mark src/test/groovy as test source root (if not already set by the gradle plugin). This uses spock/groovy for all
 tests, the world is a
 much better place now.
 
###The app uses basic auth, username and password is: `admin/admin`
 
####In order to access the Views, simply add `?view=true` to the url, i.e:
* http://localhost:8080/?view=true
* http://localhost:8080/historical?view=true

This will generate an error: http://localhost:8080/historical?day=34&view=true 
 
 Swagger can be accessed on the standard url: http://localhost:8080/swagger-ui.html
 
 Actuator also accessible on its standard url: http://localhost:8080/actuator/info

##Thanks for reviewing my code!