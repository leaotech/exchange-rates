package net.leaotech.exchangerates.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class RestConfiguration {

    @Value("${rest.connectTimeout}")
    private int connectTimeout;
    @Value("${rest.readTimeout}")
    private int readTimeout;

    @Bean
    public RestTemplate getRestTemplate() {
        // The standard JDK HTTP library does not support HTTP PATCH so we need to use the Apache HttpComponents
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(connectTimeout);
        requestFactory.setReadTimeout(readTimeout);
        return new RestTemplate(requestFactory);
    }
}