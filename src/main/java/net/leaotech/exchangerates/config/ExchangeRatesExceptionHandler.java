package net.leaotech.exchangerates.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@ControllerAdvice
public class ExchangeRatesExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> defaultHandler(Exception e) {
        log.error("Exception caught by default handler", e);
        // Do not return exception message by default as it exposes implementation
        return new ResponseEntity<>(new ExchangeRatesResponse("ERROR", INTERNAL_SERVER_ERROR.getReasonPhrase()),
                                    INTERNAL_SERVER_ERROR);
    }

    @SuppressWarnings("PMD.BeanMembersShouldSerialize")
    private static class ExchangeRatesResponse<T> {
        private String status;
        private String message;
        private T body;

        public ExchangeRatesResponse(String status, String message) {
            this.status = status;
            this.message = message;
        }
    }
}
