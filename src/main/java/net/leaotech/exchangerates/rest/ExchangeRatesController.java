package net.leaotech.exchangerates.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.leaotech.exchangerates.domain.RatesPayload;
import net.leaotech.exchangerates.service.RatesApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

@RestController
@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.DataflowAnomalyAnalysis"})
public class ExchangeRatesController {

    private RatesApiService ratesApiService;

    @Autowired
    public ExchangeRatesController(RatesApiService ratesApiService) {
        this.ratesApiService = ratesApiService;
    }

    @ApiOperation(
            value = "Retrieve today's rates",
            httpMethod = "GET",
            notes = "This endpoint will return today's rates, the latest",
            produces = "application/json; charset=utf-8"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "JSON representation of the rates.", response = RatesPayload.class),
    })
    @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getLatest(@RequestParam(value = "view", required = false, defaultValue = "false") boolean view) {
        ResponseEntity<?> latest = ratesApiService.getLatest();
        if (view) {
            String viewName = "rates";
            if (latest.getStatusCode() != HttpStatus.OK) {
                viewName = "ratesPageError";
            }
            ModelAndView mav = new ModelAndView(viewName);
            mav.addObject("rates", latest.getBody());
            return mav;
        } else {
            return latest;
        }
    }

    @ApiOperation(
            value = "Retrieve past six months rates",
            httpMethod = "GET",
            notes = "This endpoint will return the last six months worth of data for a given day",
            produces = "application/json; charset=utf-8"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "JSON representation of the rates.", response = RatesPayload.class),
    })
    @GetMapping(path = "/historical", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getHistorical(
            @RequestParam(value = "view", required = false, defaultValue = "false") boolean view,
            @RequestParam(value = "day", required = false) Integer dayOfTheMonth) {

        if (dayOfTheMonth == null) {
            dayOfTheMonth = LocalDate.now().getDayOfMonth();
        }
        ResponseEntity<?> historical = ratesApiService.getHistorical(dayOfTheMonth);
        if (view) {
            String viewName = "historical";
            if (historical.getStatusCode() != HttpStatus.OK) {
                viewName = "historicalPageError";
            }
            ModelAndView mav = new ModelAndView(viewName);
            mav.addObject("historical", historical.getBody());
            return mav;
        } else {
            return historical;
        }
    }
}
