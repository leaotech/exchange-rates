package net.leaotech.exchangerates.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RatesApiRestTemplate extends RestTemplate {

    public <T> T getForObject(String url, Class<T> clazz) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("User-Agent", "default"); //prevents cloudflare from blocking the requests
        return exchange(url, HttpMethod.GET, new HttpEntity<String>(httpHeaders), clazz).getBody();
    }
}
