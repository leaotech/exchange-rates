package net.leaotech.exchangerates.repository;

import net.leaotech.exchangerates.domain.HistoricalRates;
import net.leaotech.exchangerates.domain.RatesPayload;
import net.leaotech.exchangerates.rest.RatesApiRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//using java native data collections to retrieve and cache data
@Repository
@SuppressWarnings({"PMD.BeanMembersShouldSerialize", "PMD.SimpleDateFormatNeedsLocale", "PMD.DataflowAnomalyAnalysis"})
public class RatesRepositoryInMemoryImpl implements RatesRepository {

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    private final RatesApiRestTemplate ratesApiRestTemplate;
    @Value("${ratesapi.url}")
    private String ratesApiUrl;

    private RatesCache ratesCache;

    @Autowired
    public RatesRepositoryInMemoryImpl(RatesApiRestTemplate ratesApiRestTemplate) {
        this.ratesApiRestTemplate = ratesApiRestTemplate;
        this.ratesCache = new RatesCache();
    }

    @Override
    public RatesPayload getLatest() {
        return ratesCache.getRates(new SimpleDateFormat(DATE_FORMAT).format(new Date()), "/latest");
    }

    @Override
    public HistoricalRates getHistorical(int dayOfTheMonth) {
        return buildHistoricalRates(dayOfTheMonth);
    }

    //TODO add tests
    private HistoricalRates buildHistoricalRates(int dayOfTheMonth) {

        HistoricalRates historicalRates = new HistoricalRates();
        historicalRates.setDayOfTheMonth(dayOfTheMonth);
        List<RatesPayload> rates = new ArrayList<>();

        LocalDate initialDate = LocalDate.now();
        int monthsCount = 0;
        while (true) {
            LocalDate currentDate = initialDate.minusMonths(monthsCount).withDayOfMonth(dayOfTheMonth);
            String currentDateString = DATE_TIME_FORMATTER.format(currentDate);
            rates.add(ratesCache.getRates(currentDateString, String.format("/%s", currentDateString)));
            if (monthsCount == HistoricalRates.NUMBER_OF_MONTHS - 1) {
                break;
            }
            monthsCount++;
        }
        historicalRates.setRates(rates);
        return historicalRates;
    }

    //the cached data is small enough for this use case, more consideration required for bigger data and the jvm
    private class RatesCache {

        private Map<String, RatesPayload> cache = new HashMap<>();

        private RatesPayload getRates(String date, String ratesApiParam) {
            if (cache.containsKey(date)) {
                return cache.get(date);
            } else {
                RatesPayload ratesPayload = ratesApiRestTemplate
                        .getForObject(ratesApiUrl + ratesApiParam, RatesPayload.class);
                LocalDate localDate = LocalDate.parse(date, DATE_TIME_FORMATTER);
                ratesPayload.setMonthFriendlyName(
                        localDate.getMonth().toString().concat(String.valueOf(localDate.getYear())));
                cache.put(date, ratesPayload);
                return ratesPayload;
            }
        }
    }
}
