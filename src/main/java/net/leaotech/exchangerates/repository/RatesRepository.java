package net.leaotech.exchangerates.repository;

import net.leaotech.exchangerates.domain.HistoricalRates;
import net.leaotech.exchangerates.domain.RatesPayload;

public interface RatesRepository {

    RatesPayload getLatest();

    HistoricalRates getHistorical(int dayOfTheMonth);
}
