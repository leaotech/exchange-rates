package net.leaotech.exchangerates.domain;

public enum Currency {
    EUR, GBP, USA, HK;

    private String value;
    public String value() {
        return value;
    }
}
