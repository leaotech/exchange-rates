package net.leaotech.exchangerates.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrencyRate {

    @JsonProperty("GBP")
    String poundSterling;

    @JsonProperty("USD")
    String usDollar;

    @JsonProperty("HKD")
    String hongKongDollar;

    @JsonProperty("EUR")
    String euro;
}
