package net.leaotech.exchangerates.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties
public class HistoricalRates {

    public static final int NUMBER_OF_MONTHS = 6;

    @ApiModelProperty(example = "EUR")
    private Currency base = Currency.EUR;

    private int dayOfTheMonth;

    List<RatesPayload> rates;
}
