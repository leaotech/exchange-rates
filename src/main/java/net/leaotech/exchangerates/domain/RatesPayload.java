package net.leaotech.exchangerates.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RatesPayload {

    @ApiModelProperty(example = "EUR", notes = "The base Currency")
    private final Currency base = Currency.EUR;

    @JsonProperty("rates")
    @ApiModelProperty (notes = "each rates payload contains a set of currencies, " +
                               "their values are relative to the base currency")
    CurrencyRate currencyRate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @ApiModelProperty (notes = "the date for this particular rates payload")
    LocalDate date;

    @ApiModelProperty(notes = "intended to identify the month and year, useful for web views " +
                              "and clients consuming from this api")
    private String monthFriendlyName;
}
