package net.leaotech.exchangerates.service;

import lombok.extern.slf4j.Slf4j;
import net.leaotech.exchangerates.domain.HistoricalRates;
import net.leaotech.exchangerates.domain.RatesPayload;
import net.leaotech.exchangerates.repository.RatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@SuppressWarnings({"PMD.BeanMembersShouldSerialize"})
public class RatesApiService {

    private RatesRepository ratesRepository;

    @Autowired
    public RatesApiService(RatesRepository ratesRepository) {
        this.ratesRepository = ratesRepository;
    }

    public ResponseEntity<?> getLatest() {
        RatesPayload latest;
        try {
            latest = ratesRepository.getLatest();
            return ResponseEntity.ok(latest);
        } catch (Exception e) {
            log.error("unable to obtain latest rates data", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    public ResponseEntity<?> getHistorical(int dayOfTheMonth) {
        if (dayOfTheMonth > 0 && dayOfTheMonth < 32) { //TODO implement proper validation
            HistoricalRates historical;
            try {
                historical = ratesRepository.getHistorical(dayOfTheMonth);
                return ResponseEntity.ok(historical);
            } catch (Exception e) {
                log.error("unable to obtain historical rates data", e);
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            Map<String, String> response = new HashMap<String, String>();
            response.put("error", dayOfTheMonth + " is not a valid day of the month");
            return ResponseEntity.badRequest().body(response);
        }
    }

}
