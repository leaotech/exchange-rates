package net.leaotech.exchangerates.health;

import lombok.extern.slf4j.Slf4j;
import net.leaotech.exchangerates.rest.RatesApiRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

/**
 * We register a health check against the Rates Api as it is core for the current implementation of this app
 * Since RatesApi.io doens't seem to have a heath check endpoint, we judge based on their root path
 */
@Slf4j
@Component("rates-api-health")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class RatesApiHealthCheck implements HealthIndicator {

    private final RestTemplate restTemplate;

    @Value("${ratesapi.url}")
    private String ratesApiUrl;

    private String url;

    @Autowired
    public RatesApiHealthCheck(final RatesApiRestTemplate ratesApiRestTemplate) {
        this.restTemplate = ratesApiRestTemplate;
    }

    @Override
    public Health health() {
        url = ratesApiUrl.concat("latest");
        final int status = getServiceStatus(url);

        final Health health;
        if (status == HttpStatus.OK.value()) {
            health = Health.up()
                           .withDetail("http-status", status)
                           .withDetail("endpoint", url).build();
        } else if (status < 0) {
            health = Health.down()
                           .withDetail("reason", "Connection timed out")
                           .withDetail("endpoint", url).build();
        } else {
            health = Health.down()
                           .withDetail("http-status", status)
                           .withDetail("endpoint", url).build();
        }
        return health;
    }

    private int getServiceStatus(final String healthCheckUrl) {
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("User-Agent", "random");
            return restTemplate.exchange(healthCheckUrl, HttpMethod.GET,
                                  new HttpEntity<String>(httpHeaders), String.class).getStatusCodeValue();

        } catch (final HttpClientErrorException | HttpServerErrorException e) {
            log.error("Failed to connect to {}", healthCheckUrl, e);
            return e.getRawStatusCode();
        } catch (final ResourceAccessException e) {
            log.error("Failed to connect to {}", healthCheckUrl, e);
            return -1;
        } catch (Exception e) {
            log.error("Failed to connect to {}", healthCheckUrl, e);
            return -1;
        }
    }
}
