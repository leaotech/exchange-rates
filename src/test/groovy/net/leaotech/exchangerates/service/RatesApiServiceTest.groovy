package net.leaotech.exchangerates.service

import net.leaotech.exchangerates.domain.HistoricalRates
import net.leaotech.exchangerates.domain.RatesPayload
import net.leaotech.exchangerates.repository.RatesRepository
import net.leaotech.exchangerates.repository.RatesRepositoryInMemoryImpl
import net.leaotech.exchangerates.rest.RatesApiRestTemplate
import org.springframework.beans.factory.annotation.Value
import spock.lang.Specification

class RatesApiServiceTest extends Specification {

    @Value("\${ratesapi.url}")
    def ratesApiUrl

    RatesApiRestTemplate mockRatesApiRestTemplate = Mock(RatesApiRestTemplate)
    RatesRepository ratesRepository = Mock(RatesRepositoryInMemoryImpl)

    RatesApiService ratesApiService;

    def setup() {
        ratesApiService = new RatesApiService(ratesRepository)
    }

    def "correct Url to get rates data from rates api"() {
        given: "a corresponding expected Url"
            String expectedUrl = ratesApiUrl + "/latest"
        when: "calling get latest"
            ratesApiService.getLatest()
        then: "calls getForObject on restTemplate with expected url"
            mockRatesApiRestTemplate.getForObject(expectedUrl, _ as Class) >> new RatesPayload()
    }

    def "correct Url to get historical rates data"() {
        given: "a corresponding expected Url"
            String expectedUrl = ratesApiUrl + "/historical"
        when: "calling get latest"
            ratesApiService.getHistorical(3)
        then: "calls getForObject on restTemplate with expected url"
            mockRatesApiRestTemplate.getForObject(expectedUrl, _ as Class) >> new HistoricalRates()
    }
}
