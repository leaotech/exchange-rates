package net.leaotech.exchangerates.repository

import net.leaotech.exchangerates.domain.HistoricalRates
import net.leaotech.exchangerates.domain.RatesPayload
import net.leaotech.exchangerates.rest.RatesApiRestTemplate
import spock.lang.Specification

class RatesRepositoryTest extends Specification {

    RatesApiRestTemplate mockRatesApiRestTemplate = Mock(RatesApiRestTemplate)

    RatesRepository ratesRepository = new RatesRepositoryInMemoryImpl(mockRatesApiRestTemplate)

    def "can return the latest rates"(){
        when:
            ratesRepository.getLatest()
        then:
            1 * mockRatesApiRestTemplate.getForObject(*_) >> RatesPayload.builder().build()
    }

    def "can build hist of historical rates data" () {
        when:
            ratesRepository.getHistorical(3)
        then: "the number of calls to retrieve data from the rates api is equivalent to the number of months"
            HistoricalRates.NUMBER_OF_MONTHS * mockRatesApiRestTemplate.getForObject(*_) >> RatesPayload.builder().build()
    }
}
