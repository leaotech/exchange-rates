package net.leaotech.exchangerates.health

import net.leaotech.exchangerates.rest.RatesApiRestTemplate
import org.springframework.boot.actuate.health.Status
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.ResourceAccessException
import spock.lang.Specification

class RatesApiHealthCheckTest extends Specification {

    def mockRestTemplate = Mock(RatesApiRestTemplate)
    def healthCheck = new RatesApiHealthCheck(mockRestTemplate)

    def setup() {
        healthCheck.ratesApiUrl = "http://someUrl.com/"
    }

    def "Service reported as UP when RestTemplate response returns 200"() {
        given: "Rest template returns a 200 response"
            mockRestTemplate.exchange(*_) >> new ResponseEntity(HttpStatus.OK)
        when: "The health check is called"
            def result = healthCheck.health()
        then:
            result.status == Status.UP
            result.getDetails()["http-status"] == 200
            result.getDetails()["endpoint"] == "http://someUrl.com/latest"
    }

    def "Service reported as DOWN when it returns 404 NOT_FOUND response"() {
        given: "The remote service returns a 404 response"
            mockRestTemplate.exchange(*_) >> new ResponseEntity(HttpStatus.NOT_FOUND)
        when: "We call the health check method"
            def result = healthCheck.health()
        then: "A health object with status UP is returned"
            result.status.toString() == "DOWN"
            result.details.get("http-status") == 404
    }

    def "Service reported as DOWN when connection times out"() {
        given: "Rest template times out"
            mockRestTemplate.exchange(*_) >> { throw new ResourceAccessException("Timeout") }
        when: "The health check is called"
            def result = healthCheck.health()
        then:
            result.status == Status.DOWN
            result.getDetails()["reason"] == "Connection timed out"
            result.getDetails()["endpoint"] == "http://someUrl.com/latest"
    }

    def "Service reported as DOWN when it HttpClientServerException thrown"() {
        given: "The remote service returns a 504 response"
            mockRestTemplate.exchange(*_) >> { throw new HttpServerErrorException(HttpStatus.GATEWAY_TIMEOUT) }
        when: "We call the health check method"
            def result = healthCheck.health()
        then: "A health object with status DOWN is returned"
            result.status.toString() == "DOWN"
            result.details.get("http-status") == 504
    }
}
