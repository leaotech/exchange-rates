package net.leaotech.exchangerates.integration

import net.leaotech.exchangerates.ExchangeRatesApplication
import net.leaotech.exchangerates.domain.HistoricalRates
import net.leaotech.exchangerates.domain.RatesPayload
import net.leaotech.exchangerates.rest.ExchangeRatesController
import net.leaotech.exchangerates.service.RatesApiService
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

@SpringBootTest(classes = ExchangeRatesApplication)
@SpringBootConfiguration
class ExchangeRatesApplicationTest extends Specification {

    def ratesApiService = Mock(RatesApiService)
    def ratesApiRestController = new ExchangeRatesController(ratesApiService)

    MockMvc mockMvc;

    def setup() {
        mockMvc = standaloneSetup(ratesApiRestController).build()
    }

    def "Application launches, spins up full spring boot context"() {
        expect:
            mockMvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Unroll
    def "A page should display the exchange rates for: #description" () {

        given: "calls to retrieve either the latest or historical rates succeed"
            ratesApiService.getLatest() >> ResponseEntity.ok([] as RatesPayload)
            ratesApiService.getHistorical(*_) >> ResponseEntity.ok([] as HistoricalRates)

        when: "User opens a browser and enters the url to access the web application"
            mockMvc.perform(get(url)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andReturn().response

        then: "no exceptions"
            notThrown(AssertionError)

        and: "corresponding view loaded as per the table below"
            view().name(pageView)

        where:
            url                             | pageView             | description
            "/?view=true"                   | "rates"              | "Today's rates"
            "/historical/?view=true"        | "historical"         | "Historical rates"
    }

    @Unroll
    def "A user friendly error message displays if the application has encountered an error for: #description" () {
        given: "calls to get rates (latest or historical) contain errors"
            ratesApiService.getLatest() >> ResponseEntity.badRequest().body(["error":"unexpected error"])
            ratesApiService.getHistorical(*_) >> ResponseEntity.badRequest().body(["error":"unexpected error"])

        when: "User opens a browser and enters the url to access the web application"
            mockMvc.perform(get(url)
                   .contentType(MediaType.APPLICATION_JSON))
                    .andReturn().response

        then: "no exceptions but error view called"
            notThrown(AssertionError)
        and: "as expected, error view loaded, see the table below"
            view().name(errorPageView)
        where:
            url                      | errorPageView         | description
            "/?view=true"            | "ratesPageError"      | "Today's rates"
            "/historical?view=true"  | "historicalPageError"  | "Historical rates"
    }
}
