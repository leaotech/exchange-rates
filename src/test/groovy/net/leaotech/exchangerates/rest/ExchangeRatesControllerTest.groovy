package net.leaotech.exchangerates.rest

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import net.leaotech.exchangerates.domain.Currency
import net.leaotech.exchangerates.domain.HistoricalRates
import net.leaotech.exchangerates.service.RatesApiService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class ExchangeRatesControllerTest extends Specification {

    def ratesApiService = Mock(RatesApiService)
    def exchangeRatesController = new ExchangeRatesController(ratesApiService)

    MockMvc mockMvc = standaloneSetup(exchangeRatesController).build()

    def "can check today's exchange rates"() {
        given:
            def ratesPayload = [
                    base : Currency.EUR,
                    rates: [poundSterling : "0.8792",
                            usDollar      : "1.0867",
                            hongKongDollar: "8.422",
                            euro          : null],
                    date : "2020-04-22"
            ]
            ratesApiService.getLatest() >> [ratesPayload, HttpStatus.OK]

        when: "a request is made"
            def response = mockMvc.perform(get("/")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn().response

        then: "outputs today's exchange rates for UK USA and HK against Euro"
            notThrown(AssertionError)
            response.status == 200
            response.contentAsString == JsonOutput.toJson(ratesPayload)
    }

    def "can check for last six months of rates for a given day"() {
        given:
            def historicalJson = readFileContent("historical_rates.json")
            def dayOfTheMonth = 3
            def historicalRates = new JsonSlurper().parseText(historicalJson) as HistoricalRates

        when: "a request is made to retrieve last six month worth of data"
            ratesApiService.getHistorical(dayOfTheMonth) >> [historicalRates, HttpStatus.OK]
            def response = mockMvc.perform(get("/historical?day=" + dayOfTheMonth)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn().response

        then: "get historical exchange rates for UK, USA and HK against Euro for the past six months for the same day."
            notThrown(AssertionError)
            response.status == 200
    }

    def readFileContent(filename) {
        return this.getClass().getResource("/static/" + filename).text
    }
}
